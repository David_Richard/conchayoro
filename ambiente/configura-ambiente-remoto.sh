#!/bin/sh

REPO=https://gitlab.com/David_Richard/conchayoro.git
NODE1=ip172-18-0-10-blnemg8nddr0008imcr0@direct.labs.play-with-docker.com
NODE2=ip172-18-0-6-blnemg8nddr0008imcr0@direct.labs.play-with-docker.com
NODE3=ip172-18-0-22-blnemg8nddr0008imcr0@direct.labs.play-with-docker.com
NODE4=ip172-18-0-47-blk153id7o0g00a8q08g@direct.labs.play-with-docker.com

echo "Configuração do ambiente remoto"
  
ssh -t $NODE1 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d jenkins"

ssh -t $NODE2 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d nexus && docker-compose exec nexus sh /deploy-config/nexus/init.sh"

ssh -t $NODE3 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d sonar" 
   
echo "Configuração do ambiente remoto concluída com sucesso!!"